# PS Task
This project provisions the following:
- Jenkins server and Slave server on AWS EC2
- Nginx website hosted in docker, connected to APIGW and Dynamodb database for data persistence. Logs pushed to Cloudwatch Logs
- Cloudtrail
- Several compliance checks implemented with AWS config